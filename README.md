# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/propshop-girder

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-archived-gh-pages/#!/osrf/propshop-girder

## Until May 31st 2020, the mercurial repository can be found at

https://bitbucket.org/osrf-migrated/propshop-girder
